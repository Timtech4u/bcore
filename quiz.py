from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABb_oosAYaPBlJZMGbCTEqXAewpFPsX3OkgbX6xGIHgvrNOG_GinVosDfF9NeSC8PmucJA--s_4GX0v2YHff57431Dj8xg7qZSwpCsSAe1eCRxMDLtY6aD-R9kt9GHbeKBNLLYTlIDPS6z-m3t6HxRSCJZWP_9ZVHPi5olHScBYOf12MXk='

def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()