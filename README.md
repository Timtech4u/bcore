# Brite Core Challenge
- [X] Built using Django, DRF and Vuejs
- [X] Uses JSONField to saved Form fields and submissions.
- [X] Deployed [here](https://a7v3z2enn6.execute-api.us-west-2.amazonaws.com/dev/) on AWS Lambda using Zappa

## Description
- Created a Model for Risk Insurers with the following fields: `name, fields, submissions`
- `fields, submissions` fields are JSONFields
- This provides us a faster data structure to work with.
- ERD Diagram available [here](./Risks-ERD.png)
- Django Rest Framework was [used](https://a7v3z2enn6.execute-api.us-west-2.amazonaws.com/dev/risks)
- API was documented [here](https://a7v3z2enn6.execute-api.us-west-2.amazonaws.com/dev/docs/)
- Django Class Based Views was adopted
- Test Cases were written
- User Interface built with Vuejs and Bulma UI
- Zappa was used for deployment while maintaining AWS S3 for static files

## Build Setup

``` bash
# install dependencies
npm install

# Runserver locally
.deploy.sh
```
