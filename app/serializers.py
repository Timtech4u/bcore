from rest_framework.serializers import ModelSerializer, JSONField
from .models import Risk

class RiskSerializer(ModelSerializer):
    fields = JSONField()
    submissions = JSONField(default=[])

    class Meta:
        model = Risk
        fields = '__all__'