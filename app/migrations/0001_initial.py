# Generated by Django 2.0.7 on 2018-12-06 16:28

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Risk',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('insurer', models.CharField(blank=True, max_length=50, null=True)),
                ('added_on', models.DateTimeField(auto_now_add=True)),
                ('fields', jsonfield.fields.JSONField()),
                ('submissions', jsonfield.fields.JSONField(blank=True)),
            ],
            options={
                'verbose_name': 'Insurance Risk',
                'verbose_name_plural': 'Insurance Risks',
            },
        ),
    ]
