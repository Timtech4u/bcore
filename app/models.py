from __future__ import unicode_literals
from django.db import models
from jsonfield import JSONField

class Risk(models.Model):
    insurer = models.CharField(max_length=50, blank=True, null=True)
    added_on = models.DateTimeField(auto_now_add=True)
    fields = JSONField()
    submissions = JSONField(blank=True)

    def __str__(self):
        return "{}".format(self.id)

    class Meta:
        verbose_name = "Insurance Risk"
        verbose_name_plural = "Insurance Risks"
