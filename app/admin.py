from django.contrib import admin
from .models import Risk

@admin.register(Risk)
class RiskAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            "fields": (
                ['insurer']
            ),
        }),
        ("JSON Data", {"fields": ["fields", "submissions"]})
    )
    readonly_fields = ("fields", "submissions")
