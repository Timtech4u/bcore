import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from app.views import index
from app.models import Risk
from app.serializers import RiskSerializer


# initialize the APIClient app
client = Client()

class GetAllRisksTest(TestCase):
    """ Test module for GET all Risks API """

    def setUp(self):
        Risk.objects.create(
            name='test', fields=[], submissions=[])
        Risk.objects.create(
            name='sample', fields=[], submissions=[])

    def test_get_all_Risks(self):
        # get API response
        response = client.get(reverse('get_all_risks'))
        # get data from db
        Risks = Risk.objects.all()
        serializer = RiskSerializer(Risks, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleRiskTest(TestCase):
    """ Test module for GET single Risk API """

    def setUp(self):
        Risk.objects.create(
            name='test', fields=[], submissions=[])
        Risk.objects.create(
            name='sample', fields=[], submissions=[])

    def test_get_valid_single_Risk(self):
        response = client.get(
            reverse('get_all_risks', kwargs={'pk': self.test.pk}))
        risk = Risk.objects.get(pk=self.test.pk)
        serializer = RiskSerializer(risk)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_Risk(self):
        response = client.get(
            reverse('get_all_risks', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewRiskTest(TestCase):
    """ Test module for inserting a new Risk """

    def setUp(self):
        self.valid_payload = {
            'name': 'Dangote',
            'fields': [
                {
                    "name": "Fullname",
                    "type": "text"
                },
                {
                    "name": "Age",
                    "type": "number"
                }
            ],
            'submissions': []
        }

        self.invalid_payload = {
            'name': 'Faker',
            'fields': "[
                {
                    "name": "Fullname",
                    "type": "text"
                }
            ]",
            'submissions': "[]"
        }

    def test_create_valid_risk(self):
        response = client.post(
            reverse('get_all_risks'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_Risk(self):
        response = client.post(
            reverse('get_all_risks'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestView(TestCase):
    """Test the views of the application"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_index_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
