from .models import Risk
from .serializers import RiskSerializer
from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView

class ListCreateRisk(ListCreateAPIView):
    queryset = Risk.objects.all()
    serializer_class = RiskSerializer

def index(request):
    return render(request, 'index.html')